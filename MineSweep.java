import java.util.Scanner;
import java.util.Random;
import java.util.Arrays;
	
	

// ============================================================
// -----------------------MINESWEEP CLASS----------------------
// ============================================================

class MineSweep {
	public static final String INFO = "\nA minesweeper game.\n##################\n\nWarning: If you mark the supposedly non-bomb tile, you lose!\nExample command: '1 1 o' or '2 2 m'\n";
	private int mineCount, row, col;
	private Mine[][] land;
	private int[][] mineLocation;
	private String landStatus;
	private boolean life = true;
	
	public static void main (String[] args) {
		int[] MRC = new int[3];
		boolean isAlive, isCompleted;
		Scanner sc = new Scanner (System.in);
		
		System.out.println(INFO);
		do{
			try{
				System.out.print("(min: 1) How many mines you want? ");
				MRC[0] = Integer.parseInt(sc.nextLine());
				System.out.print("(min: 2) How many row? ");
				MRC[1] = Integer.parseInt(sc.nextLine());
				System.out.print("(min: 2) How many column? ");
				MRC[2] = Integer.parseInt(sc.nextLine());
				
				if(MRC[0] < 1 || MRC[1] < 2 || MRC[2] < 2) System.out.println("\nYou entered a non desired number.\n");
			} catch (Exception e) {continue;}
		} while (MRC[0] < 1 || MRC[1] < 2 || MRC[2] < 2);
		
		MineSweep game = new MineSweep(MRC[0], MRC[1], MRC[2]);
		isAlive = true;
		isCompleted = false;
		
		while (isAlive || isCompleted) {
			int r = 0;
			int c = 0;
			String option = "";
			String[] rcString;
			
			System.out.println(game.getLandStatus()); // showing the board
			do{
				try{
					System.out.println("Which row (space) col (space) option to click: ");			
					rcString = sc.nextLine().split(" ");
					r = Integer.parseInt(rcString[0])-1;
					c = Integer.parseInt(rcString[1])-1;
					option = rcString[2];
				} catch(Exception e) {continue;}
			}while ( r < 0 || c < 0 || r > MRC[1] || c > MRC[2] &&(!(option.equalsIgnoreCase("m")) || !(option.equalsIgnoreCase("o"))));
			
			game.update(r, c, option);
			isAlive = game.getLife();
			isCompleted = game.checkIfWin();
			
			if (!isAlive) System.out.println(game.getLandStatus());			
		}		
	}
	
	public MineSweep(int mineCount, int row, int col) {
		this.row = row;
		this.col = col;
		this.mineCount = mineCount;
		this.mineLocation = generateMineLocation();
		generateLand();
		this.landStatus = generateView();
	}
	
	public void generateLand() {
		this.land = new Mine[this.row][this.col];
		int[] dump = new int[2];
		for (int r = 0; r < this.row; r++) {
			dump[0] = r;
			for(int c = 0; c < this.col; c++) {				
				dump[1] = c;
				if (compareIfContaining(this.mineLocation, dump)) {
					this.land[r][c] = new Mine(true);
				} else this.land[r][c] = new Mine(false);	
			}			
		}
		for (int r = 0; r < this.row; r++) {
			for(int c = 0; c < this.col; c++) {				
				if(this.land[r][c].getValue() == 9) generateSurrounding(r, c);	
			}			
		}
	}
	
	public boolean compareIfContaining(int[][] big, int[] small) {
		boolean isContaining = false;
		for (int[] x : big) {
			if (Arrays.equals(x, small)) isContaining = true;
		}
		return isContaining;
	}
	
	public void update(int row, int col, String option) {
		String result = "";
		boolean mineNotStepped = true;
		if (option.equalsIgnoreCase("o")) {
			this.land[row][col].setOpened(true);
			if(this.land[row][col].getValue() == 9) {
				this.life = false;
				result = "\n################################################\n" + generateView() + "\nYOU LOSE!";
			}
			else result = generateView();
		} else if(option.equalsIgnoreCase("m")) {
			this.land[row][col].setMarked(true);
			if(!validateMarkedIsBomb(new int[] {row, col})) {
				this.life = false;
				result = "\n################################################\n" + generateView() + "\nYOU LOSE! WRONG MARK :V";
			} else {
				result = generateView();
				this.mineCount--;
			}
			
		}
		this.landStatus =  result;
	}
	public boolean checkIfWin() {
		boolean win = false;
		if(mineCount == 0 ) win = true;
		return win;
	}
	public boolean validateMarkedIsBomb(int[] piece) {
		boolean c = false;
		if(compareIfContaining(this.mineLocation, piece)) {
			c = true;
		}
		return c;
	}
	
	public String generateView() {
		String result = "";
		for (int r = 0; r < this.row; r++) {
					for(int c = 0; c < this.col; c++) {				
						result += this.land[r][c].getSurface();
					}
				result += "\n";
				}
		return result;
	}
	
	public void generateSurrounding(int r, int c) {
		int rm = r - 1; // r minus
		int rp = r + 1; // r plus
		int cm = c - 1;
		int cp = c + 1;
		evalMineIndicator(rm,cm);
		evalMineIndicator(rm,c);
		evalMineIndicator(rm,cp);
		evalMineIndicator(r,cm);
		evalMineIndicator(r,cp);
		evalMineIndicator(rp,cm);
		evalMineIndicator(rp,c);
		evalMineIndicator(rp,cp);		
	}
	
	public void evalMineIndicator(int r, int c) {
		if( r < 0 || c < 0 || r >this.row-1 || c >this.col-1 ) return;
		this.land[r][c].addValue(); // whether it's mine or not is checked in Mine class.
	}
	
	public int[][] generateMineLocation() {
		Random rand = new Random();
		int index = 0;
		int[][] mineLoc = new int[this.mineCount][2];
		
		while( index < this.mineCount) {		
			int[] rc = new int[]{rand.nextInt(this.row), rand.nextInt(this.col)};
			if(!compareIfContaining(mineLoc,rc)) {
				mineLoc[index] = rc;
				index ++;
			}			
		}
		return mineLoc;
	}
	
	public String getLandStatus() {return this.landStatus;}
	
	public boolean getLife() {return this.life;}
}

// ============================================================
// -------------------------MINE CLASS-------------------------
// ============================================================

class Mine{
	private int value;
	private boolean isOpened = false;
	private boolean isMarked = false;
	public Mine(boolean isBomb) {
		if(isBomb) this.value = 9;
		else this.value = 0;
	}
	
	public int getValue() {return this.value;}
	
	public boolean getOpened() {return this.isOpened;}
	
	public String getSurface() {
		if (this.isMarked) return " M ";
		else if(this.isOpened) return " " + String.valueOf(this.value) + " ";
		else return " - ";
	}
	
	public void setOpened(boolean isOpened) {this.isOpened = isOpened;}
	
	public void setMarked(boolean isMarked) {this.isMarked = isMarked;}
	
	public void addValue() {
		if(this.value != 9) this.value += 1;		
	}
	
	
}



